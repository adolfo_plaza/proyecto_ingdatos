<div id="header">
  <img src="https://www.ufro.cl/index.php/normas-corporativas/31-logo-ufro-azul-vertical/file" width="100"/>
</div>


## Análisis de Factores influyentes en los accidentes de tráfico en el Reino Unido

Autores:
-Nicolás Felipe Gómez Peréz
-Adolfo Esteban Plaza Hernández
-Alejandro Daniel Valenzuela Riveros

## Descripción
El objetivo de este proyecto es aprender acerca de los métodos de exploración de datos y técnicas de minería de datos.

Este proyecto consta de un análisis exploratorio de datos, específicamente de los factores que nos encargó el ministerio de transporte de Reino Unido, así como también se desarrollaron dos experimentos relacionados con la minería de datos que tuvieran relación con encontrar patrones relacionados a la severidad de los accidentes y si es que estos se pueden predecir en base a diversos factores, para así poder ayudar a evitar más tragedias.

## Instalación y carga de dataset
Para cargar los datos correctamente se puede descargar el dataset desde kaggle:
https://www.kaggle.com/datasets/akshay4/road-accidents-incidence
Tambien se puede cargar desde Github:
https://raw.githubusercontent.com/AleVZR/df/main/uk_road_acc.csv
Para poder ejecutar el hito 1 recomendamos abrirlo con Rstudio, y para el hito 2 y 3 usamos usar Jupyter Anaconda.
